package Task17;

import java.util.Comparator;

public class AgeComperator implements Comparator<Man> {

    @Override
    public int compare(Man person1, Man person2) {
        if (person1.getAge()==person2.getAge())
        return 0;
        else if (person1.getAge()>person2.getAge()){
                return 1;}
        else {
            return -1;}
        }

    }

