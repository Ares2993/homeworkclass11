package Task17;

import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class GenericComparator {

    public static void main(String[] args) {
        Set<Man> mans = Set.of(
                new Man("David", 25),
                new Man("Kate", 22),
                new Man("Harry", 34),
                new Man("Annabele", 13));


        List<Man> sortedMans = sort(mans, new AgeComperator());
        System.out.println(sortedMans);

        List<Integer> ints = List.of(10, 20, 30, 40);
        List<Integer> reversedList = sort(ints, Comparator.reverseOrder());
        System.out.println(reversedList);

    }

    static <T> List<T> sort(Collection<T> input, Comparator<T> comparator) {

         List<T>arrayList=new ArrayList<>(input);
         arrayList.sort(comparator);

       return arrayList;
}




}
