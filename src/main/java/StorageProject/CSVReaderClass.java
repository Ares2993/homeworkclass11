package StorageProject;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class CSVReaderClass {



    public static void main(String[]args) throws IOException {
        List<Product>list_of_products=readProducts();

        for(Product p:list_of_products){
            System.out.println(p);
        }
    }


    private static List<Product> readProducts() throws IOException {
        List<Product> list_of_products=new ArrayList<>();
        Path pathToFile= Paths.get("src/main/java/StorageProject/Product.csv");


        try(BufferedReader br= Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)){

            //reading first line from the text file
            String line=br.readLine();



            //reading of all next line util the list is over
            while(line != null) {


                //separation of values in the file by using string.split and a ; as the delimiter
                String[] attributes=line.split(";");
                {
                    for (int i = 3; i <list_of_products.size() ; i++) {
                        String[] attributes2=line.split(":");
                    }

            }



                Product product=createProduct(attributes);

                //adding products into ArrayList
                list_of_products.add(product);


                //read next line before looping until the end of file
                line=br.readLine();
            }

        }catch(IOException ioe){
            ioe.printStackTrace();
        }

        return list_of_products;
    }





    private static Product createProduct( String[] metadata){

        int nr_of_entry= parseInt(metadata[0]);
        String operation=metadata[1];
        String timestamp=metadata[2];
        int product_id= parseInt(metadata[3]);
        int count_of_product=parseInt(metadata[4]);
        String name = metadata[4];


          //creation and return of product of metadata
        return new Product(nr_of_entry,operation,timestamp,product_id,count_of_product,name);
    }
}



