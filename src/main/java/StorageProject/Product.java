package StorageProject;

public class Product {

    private int nr_of_entry;
    private String operation;
    private String timestamp;
    private int product_id;
    private int count_of_product;
    private String name;



    Product(int nr_of_entry, String operation, String timestamp, int product_id, int count_of_product, String name){

        this.nr_of_entry=nr_of_entry;
        this.operation=operation;
        this.timestamp=timestamp;
        this.product_id=product_id;
        this.count_of_product=count_of_product;
        this.name=name;

    }

    @Override
    public String toString() {
        return "Nr of entry=" + nr_of_entry + "Add/Remove" + operation + "Date of last operation" + timestamp + "name of product(ID:" + product_id + "):" + name + ":" +count_of_product;
    }
}
