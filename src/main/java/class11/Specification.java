package class11;

public class Specification extends ComputerFactory {

    @Override
    protected void installProcessor(Processor proc) {
        System.out.println("chosen processor: AmdProcessor ");
    }

    @Override
    protected void instalGPU(Gpu gpu) {
        System.out.println("chosen GPU: RadeonGpu ");
    }

}
