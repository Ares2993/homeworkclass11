package class11;

public class ComputerFactory extends Processor {

    private Processor proc;
    private Gpu gpu;

    void installProcessor(Processor proc) {
        System.out.println("instalation of processor in our computer");
    }

    int getProcessor() {
        return getMHz();
    }

    void instalGPU(Gpu gpu) {
        System.out.println("instalation of GPU");

    }

    int getGPU() {
        return getG3dMark();
    }

    @Override
    protected int getMHz() {
        return 5000;
    }

//    TODO
//    @Override
    protected int getG3dMark() {
        return 7900;
    }
}
