package class11;

public abstract class Processor {

    abstract protected int getMHz();

    void processor() {
        getMHz();
    }
}