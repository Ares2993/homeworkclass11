package class11;

public class MainClass {

    public static void main(String[] args) {

        System.out.println("computer number 1");
        ComputerFactory computer1 = new ComputerFactory();
        computer1.getMHz();
        computer1.getG3dMark();

        System.out.println("computer number 2");
        ComputerFactory computer2 = new ComputerFactory();
        computer2.getMHz();
        computer2.getG3dMark();

        System.out.println("computer number 3");
        ComputerFactory computer3 = new ComputerFactory();
        computer3.getMHz();
        computer3.getG3dMark();
    }
}
