package Task15;

import com.github.javafaker.Faker;

import java.util.*;


public class Task15mainClass {
    private static final int PERSON_NUMBER = 5;


    public static void main(String[]args){

        List<Person>employees=new LinkedList<Person>();

        Person worker1=new Person("John",34);
        Person worker2=new Person("John",34);
        Person worker3=new Person("Henry",34);
        Person worker4=new Person("Suzzane",23);
        Person worker5=new Person("Angie",42);
        Person worker6=new Person("Elizabeth",31);
        Person worker7=new Person("Sam",19);
        Person worker8=new Person("Theodore",67);
        Person worker9=new Person("Aragorn",79);
        Person worker10=new Person("Aragorn",53);


        employees.add(worker1);
        employees.add(worker2);
        employees.add(worker3);
        employees.add(worker4);
        employees.add(worker5);
        employees.add(worker6);
        employees.add(worker7);
        employees.add(worker8);
        employees.add(worker9);
        employees.add(worker10);


        Faker faker=new Faker();
        for (int i = 0; i < PERSON_NUMBER; i++) {
            employees.add(
                    new Person(
                            faker.name().firstName(),
                            faker.number().numberBetween(10, 80)
                    )
            );
        }




        for(Person name: employees){                                   //creation of the for loop
            System.out.println(name);


        }

        Set<Person> uniqueEmployees = new HashSet<>(employees);        //creation set from the list

        System.out.println("employees"+uniqueEmployees);

        Map<String, Person> nameToPerson = new HashMap<>();            //creation of a map




        for (int i = 0; i < employees.size(); i++) {                   // Wykorzystujemy pętlę która iteruje po indeksach list "employees".
            Person empl = employees.get(i);                           // wyciągamy wartość (encję) z listy pod aktualnym indeksem i ( 0 <= i <= wielkość list)
            nameToPerson.put(empl.getName(), empl);                   // wkładamy do naszej mapy klucz oraz wartość. Kluczem jest imię aktualnego pracownika. Wartością jest sam pracownik

            System.out.println("Data of employees:"+nameToPerson);


            System.out.println(uniqueEmployees.size());
            System.out.println(nameToPerson.size());
            System.out.println(uniqueEmployees.size() == nameToPerson.size());






        }












    }
}
