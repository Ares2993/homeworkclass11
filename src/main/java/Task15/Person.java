package Task15;

import java.util.*;

public class Person {

    String name;
    int age;
    List<Person> employees;
    Set<Person> uniqueEmployees;
    Map<String, Person> nameToPerson;



    Person(String name, int age){
        this.name=name;
        this.age=age;

    }

    String getName() {
        return name;
    }



    public int getAge(){
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {     //sprawdza czy referencja "o" wskazuje na instancję która jest "is a" instancją typu Person lub pochodną (dzieckiem)
            return false;
        }
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}







