package HomeTask13;

import java.util.List;

public class MainClass {

    public static void main(String[]args){

        Person father=new Person("Zbyszek",50);
        Person mother=new Person("Karyna",44);
        Person son=new Person("Brajan",12);


        father.setRelatives(List.of(mother, son));
        mother.setRelatives(List.of(father, son));
        son.setRelatives(List.of(father, mother));


        System.out.println(father.toString());
        System.out.println(mother.toString());
        System.out.println(son.toString());







    }


}
