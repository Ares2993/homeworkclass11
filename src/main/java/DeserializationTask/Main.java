package DeserializationTask;

import com.google.gson.Gson;

import java.io.IOException;

import java.lang.NumberFormatException;

public class Main {

    public static void main(String[] args) {

        System.out.println("Serialization and Deserialization task");

        //   serialization();


            deserialization();


    }

    private static void serialization() {

        Orders orders = new Orders(
                "1",
                "2011-12-03T10:15:30Z",
                "123",
                "Sue",
                "Hatfield",
                "New York");

        //Serialization
        Gson gson = new Gson();
        String json = gson.toJson(orders);

    }

    private static void deserialization() throws NumberFormatException {
        try {
            String ordersJson = "{'order_no':1,'date':2011-12-03T10:15:30Z,'cust_id':123,'first_name':Sue,'last_name':Hatfield,'city':New York}";

            Gson gson = new Gson();
            Orders orders = gson.fromJson(ordersJson, Orders.class);
        } finally {
            System.out.println("weird data");
        }
    }
}