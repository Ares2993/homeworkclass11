package DeserializationTask;



public class Orders {

    private int order_no;
    private int date;
    private String customer;
    private int cust_id;
    private String first_name;
    private String last_name;
    private String city;

    public Orders(String order_no, String date, String cust_id, String first_name, String last_name, String city) {
    }

    public void setOrder_no(int order_no) {
        this.order_no = order_no;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setCust_id(int cust_id) {
        this.cust_id = cust_id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getOrder_no() {
        return order_no;
    }

    public int getDate() {
        return date;
    }

    public String getCustomer() {
        return customer;
    }

    public int getCust_id() {
        return cust_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getCity() {
        return city;
    }
}




