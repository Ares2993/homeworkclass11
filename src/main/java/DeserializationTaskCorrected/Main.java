package DeserializationTaskCorrected;

import com.google.gson.Gson;

public class Main {

    public static void main(String[] args) {
        System.out.println("Serialization and Deserialization task");

        final Orders orders = new Orders(
                1,
                "2011-12-03T10:15:30Z",
                "custormer_123",
                123,
                "Sue",
                "Hatfield",
                "New York");

        System.out.println("To serialization: " + orders);
        String serializedOrders = serialization(orders);

        System.out.println("Serialized: " + serializedOrders);
        Orders deserializedOrders = deserialization(serializedOrders);

        System.out.println("Deserialized" + deserializedOrders);
    }

    private static String serialization(Orders orders) {
        //Serialization
        Gson gson = new Gson();
        return gson.toJson(orders);
    }

    private static Orders deserialization(String serializedOrders) {
        Gson gson = new Gson();
        return gson.fromJson(serializedOrders, Orders.class);
    }
}
