package DeserializationTaskCorrected;

public class Orders {

    private int orderNo;
    private String date;
    private String customer;
    private int custId;
    private String firstName;
    private String lastName;
    private String city;

    public Orders(int orderNo, String date, String customer, int custId, String firstName, String lastName, String city) {
        this.orderNo = orderNo;
        this.date = date;
        this.customer = customer;
        this.custId = custId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderNo=" + orderNo +
                ", date=" + date +
                ", customer'" + '\'' +
                ", custId=" + custId +
               ", firstName='" + firstName + '\'' +
               ", lastName='" + lastName + '\'' +
                ", city='" + city + '\'' +
               '}';
    }
}
