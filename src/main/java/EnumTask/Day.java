package EnumTask;

public enum Day {

    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,FRIDAY,
    SATURDAY{
        public float calculateDailyPay(int basicDailyPay) {
            return 1.5F * basicDailyPay;
        }

    },
    SUNDAY {
        public float calculateDailyPay(int basicPayDay) {
            return 2F * basicPayDay;
        }
    };

    private final float payFactor = 1.0F;

    public float calculateDailyPay(int basicPayDay) {
        return payFactor * basicPayDay;
    }

    @Override
    public String toString() {
        return this.name() + " ";
    }

    }



