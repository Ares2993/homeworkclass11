package EnumTask;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        workingDays labourWorkingDays = new workingDays() {

            @Override
            public List<Day> getDays() {
                return List.of(Day.MONDAY, Day.TUESDAY, Day.WEDNESDAY, Day.THURSDAY, Day.FRIDAY);
            }

            @Override
            public String toString() {
                return printDays();
            }
        };

       workingDays emergencyWorkingDays = new EmergencyWorkingDays();

        Person doctor = new Person("Doctor", emergencyWorkingDays, 1000);
        Person mechanic = new Person("Mechanic", labourWorkingDays, 500);

        System.out.println(doctor);
        System.out.println(mechanic);

    }

    private static class EmergencyWorkingDays implements workingDays {

        @Override
        public List<Day> getDays() {
            return List.of(Day.values());
        }

        @Override
        public String toString() {
            return printDays();
        }
    }
}


