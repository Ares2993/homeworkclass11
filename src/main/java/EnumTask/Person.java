package EnumTask;

public class Person {

    public  String name;
    public  workingDays workingDays;
    public  int basicDailyPay;

    public Person(String name, EnumTask.workingDays workingDays, int basicDailyPay) {
        this.name = name;
        this.workingDays = workingDays;
        this.basicDailyPay = basicDailyPay;
    }

    public Person() {

    }


    public float calculateWeeklySalary() {
        int salary = 0;
        for (Day day : workingDays.getDays()) {
            salary += day.calculateDailyPay(basicDailyPay);
        }
        return salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", basicDailyPay=" + basicDailyPay +
                ", workingDays=" + workingDays +
                ", weeklySalary=" + calculateWeeklySalary() +
                '}';
    }

}
