package EnumTask;

import java.util.List;
import java.util.stream.Collectors;

public interface workingDays {

    List<Day>getDays();

    default String printDays() {
        return String.join(" ", getDays().stream().map(Day::toString).collect(Collectors.toList()));
    }
}
