package Task16;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;



public class Person {

    private String name;
    private String surname;
    public int age;
    private Set<Person> familyTreeSet = new TreeSet<>(new AgeComperator());
    private Set<Person> familyHashSet = new HashSet<>();


    public Person(String name, String surname, int age) {
        this.name = name;
        this.age = age;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public int getAge() {
        return age;
    }


    void addFamilyMember(Person relative) {
        familyTreeSet.add(relative);
        familyHashSet.add(relative);
    }

    public SortedSet<Person> getFamilyTreeSet() {
        return (SortedSet<Person>) familyTreeSet;
    }

    public List<Person> getFamilySorted() {                                  //to sortuje liste people
        List<Person> list = new ArrayList(familyHashSet);
        Collections.sort(list, new AgeComperator());
        return list;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", relatives=" + familyTreeSet +    // TODO infinitive loop cause
                ", relatives=" + convertRelatives(familyTreeSet) +      //aby nie doszło do infinitive loop
                '}';
    }


    private String convertRelatives(Set<Person> relatives) {             //aby nie doszło do infinitive loop
        String out = "";
        for (Person relative : relatives) {
            out = out + relative.name + relative.age;
        }

        return out;
    }

    @Override
    public boolean equals(Object o) {                    //cały override aby wyłapał członków rodziny
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {                           //sprawdza czy referencja O wskazuje na instancje typu Person
            return false;
        }
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname);
    }

    @Override
    public int hashCode() {                                //po co nam ten hashcode?
        return Objects.hash(name, surname, age);
    }



}







