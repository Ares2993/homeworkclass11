package carProduction;

public interface engine {

    int getHorsepower();
    int getPrice();

    default void printEngineInfo() {
        System.out.println("engine {"+"horsepower"+getHorsepower()+'}');

    }
}
