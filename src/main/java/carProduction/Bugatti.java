package carProduction;

public class Bugatti extends multiEngines {
    @Override
    public int getHorsepower() {
        return getNrOfEngines()*300;
    }

    @Override
    public int getPrice() {
        return 1000000;                     //full price of the car
    }

    @Override
    public int getEngineSize() {
        return 5;
    }
}
