package carProduction;

public interface car extends engine,engineCapacity {

    default void printBasicInfo() {
        System.out.println("Basic information for"+this.getClass().getSimpleName()+
                "Horsepower:"+getHorsepower()+"Engine litrage:"+getEngineSize()+
                "Whole car price:"+getPrice());
    }
}
