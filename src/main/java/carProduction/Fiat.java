package carProduction;

public class Fiat implements car {
    @Override
    public int getHorsepower() {
        return 90;
    }

    @Override
    public int getPrice() {
        return 50000;                            //full price of the car
    }

    @Override
    public int getEngineSize() {
        return 1;
    }
}
