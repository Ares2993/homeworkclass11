package carProduction;

public interface engineCapacity {

    int getHorsepower();
    int getPrice();
    int getEngineSize();
}
