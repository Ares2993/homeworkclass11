package StorageProject2;

import StorageProject.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;

import static java.lang.Integer.parseInt;

public class CSVReaderClass2 {
    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}




    public static void main(String[]args) throws IOException {
        List<Product2>list_of_products=readProducts();

        for(Product2 p:list_of_products){
            System.out.println(p);
        }
    }


    private static List<Product2> readProducts() throws IOException {
        List<Product2> list_of_products=new ArrayList<>();
        Path pathToFile= Paths.get("src/main/java/StorageProject2/Product2.csv");


       try{
           FileReader fileReader=new FileReader(String.valueOf(pathToFile));

           CSVReader csvReader=new CSVReader(fileReader);
           String[] nextRecord;

           while((nextRecord=csvReader.readNext()) != null) {
               for (String cell : nextRecord) {
                   System.out.println(cell);
               }
           }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }

        return list_of_products;
    }

    private static Product2 createProduct2(String[]metadata){

        int nr_of_entry= parseInt(metadata[0]);
        String operation=metadata[1];
        String timestamp=metadata[2];
        int product_id= parseInt(metadata[3]);
        int count_of_product=parseInt(metadata[4]);
        String name = metadata[4];


        //creation and return of product of metadata
        return new Product2(nr_of_entry,operation,timestamp,product_id,count_of_product,name);
    }
}

