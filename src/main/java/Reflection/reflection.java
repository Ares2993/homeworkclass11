package Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;

public class reflection {

    public void getConstructorName() throws ClassNotFoundException {
        Class<?> BugattiClass = Class.forName("carProduction.Bugatti");

        Constructor<?>[] constructors = BugattiClass.getConstructors();


        System.out.println(Arrays.toString(constructors));
    }


    public void getMethodsName() throws ClassNotFoundException {

        Class<?> BugattiClass = Class.forName("carProduction.Bugatti");
        Method[] methods = BugattiClass.getDeclaredMethods();


        System.out.println(Arrays.toString(methods));


    }








    }
